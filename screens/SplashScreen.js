import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import * as Animatable from 'react-native-animatable';


import {
    View,
    Text,
    Button,
    StyleSheet,
    StatusBar,
    Dimensions,
    TouchableOpacity,
    Image
} from 'react-native';


const SplashScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#004aad' barStyle="light-content" />

            <View style={styles.header}>
                {/* <Text>Header</Text> */}
                <Animatable.Image
                    animation="tada"
                    // duration="1500"
                    source={require('../assets/logo.png')}
                    style={styles.logo}
                    resizeMode="stretch"
                />

            </View>
            <Animatable.View style={styles.footer}>
                <Text style={styles.title}>Welcome to Fifa Generator</Text>
                <Text style={styles.textg}>Sign in with account</Text>
                <View style={styles.button}>

                    <TouchableOpacity onPress={() => navigation.navigate('SignInScreen')}>
                        <LinearGradient
                            colors={['#004aad', '#01ab9d']}
                            style={styles.signIn}>
                            <Text style={styles.text}>Get Started></Text>

                            <MaterialIcons
                                name="navigate-next"
                                color="#fff"
                                size={20}
                            />
                        </LinearGradient>

                    </TouchableOpacity>
                </View>

            </Animatable.View >

        </View >
    );
};

export default SplashScreen;

const { height } = Dimensions.get("screen");
const height_logo = height * 0.28;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#004aad'
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        flex: 1,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30
    },
    logo: {
        width: height_logo,
        height: height_logo
    },
    title: {
        color: '#05375a',
        fontSize: 30,
        fontWeight: 'bold'
    },
    text: {
        color: '#fff',
        marginTop: 5
    },
    textg: {
        color: 'grey',
        marginTop: 5
    },
    button: {
        alignItems: 'flex-end',
        marginTop: 30
    },
    signIn: {
        width: 150,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        flexDirection: 'row'
    },
    textSign: {
        color: 'white',
        fontWeight: 'bold'
    }
});